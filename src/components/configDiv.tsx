/**
 * configDiv.tsx
 * This file defines the React component `ConfigDiv` which displays the parameters of the grid interface.
 * @prop none - This component does not accept any props but uses the context to access the values needed.
 * @export ConfigDiv
 */

import { useEffect, useState, useRef } from 'react';
import { Slider, SliderChangeEvent } from 'primereact/slider';
import { InputText } from 'primereact/inputtext';
import { InputNumber } from 'primereact/inputnumber';
import { Button } from 'primereact/button';
import { OverlayPanel } from 'primereact/overlaypanel';
import { ColorPicker } from 'primereact/colorpicker';
import ScreenCaptureModal from './modals/screenCapture';

import { useAppContext, AppContextType } from '../context';


function ConfigDiv() {

    console.log('ConfigDiv render');

    // Import the context
    const { isFirstRender, config, grid } = useAppContext() as AppContextType;

    // Get the dim from the config (will be 10 on first render and the updated if config is changed with load from local storage)
    const [dim, setDim] = useState(config?.width || 10);
    const [posX, setPosX] = useState(config?.startPosX || 0);
    const [posY, setPosY] = useState(config?.startPosY || 0);
    const [angle, setAngle] = useState(config?.startAngle || 0);
    const [penIcon, setPenIcon] = useState(config?.penIcon || "arrow");
    const [penColor, setPenColor] = useState(config?.penColor || "ff0000");
    const opDim = useRef<OverlayPanel>(null);
    const opPos = useRef<OverlayPanel>(null);
    const opPen = useRef<OverlayPanel>(null);

    const [dlgCaptureVisible, setdlgCaptureVisible] = useState(false);

    // Update the dim when the config is changed
    useEffect(() => {
        // If it's the first render, return
        if (isFirstRender) {
            return;
        }
        if (config) {
            // Set the dim from the config
            setDim(config.width);
            // Set the position from the config
            setPosX(config.startPosX);
            setPosY(config.startPosY);
            // Set the angle from the config
            setAngle(config.startAngle);
            // Set the pen icon from the config
            setPenIcon(config.penIcon);
            // Set the pen color from the config
            setPenColor(config.penColor);
        }
    }, [isFirstRender, changeGridDim]);

    // Handle slider change
    const handleSliderChange = (e: SliderChangeEvent) => {
        const newDim = e.value as number;
        setDim(newDim);
        changeGridDim('width', newDim);
    };

    // Handle input change
    const handleInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const newDim = parseInt(e.target.value);
        setDim(newDim);
        changeGridDim('width', newDim);
    };

    // Handle pen start position change
    const handlePenStartPosXChange = (e: { value: number | null }) => {
        const newPosX = e.value as number;
        setPosX(newPosX);
        changePenStartPos('startPosX', newPosX);
    };

    const handlePenStartPosYChange = (e: { value: number | null }) => {
        const newPosY = e.value as number;
        setPosY(newPosY);
        changePenStartPos('startPosY', newPosY);
    };



    /**
     * ChangeGridDim
     * Function to change the grid dimension (when the slider or input is changed in ConfigDiv component)
     * @param key - The key of the config to change
     * @param value - The new value
     **/
    function changeGridDim(key: string, value: number) {
        if (config !== null) {
            config.set(key, value);
            // Update the grid
            if (grid !== null) {
                grid.setDim(config.width);
            }
        }
    }

    /**
     * ChangePenStartPos
     * Function to change the pen start position
     * @param key - The key of the config to change
     * @param value - The new value
     */
    function changePenStartPos(key: string, value: number) {
        if (config !== null) {
            config.set(key, value);
            // Update the grid
            if (grid !== null) {
                grid.changePenStartPos(config.startPosX, config.startPosY);
            }
        }
    }

    /**
     * ChangePenStartAngle
     * Function to change the pen start angle
     * @param key - The key of the config to change
     * @param value - The new value
     * @returns void
     **/
    function changePenStartAngle(key: string, value: number) {
        if (config !== null) {
            config.set(key, value);
            // Update the grid
            if (grid !== null) {
                grid.changePenStartAngle(config.startAngle);
            }
        }
    }

    /**
     * ChangePenIcon
     * Function to change the pen icon
     * @param key - The key of the config to change
     * @param value - The new value
     * @returns void
     */
    function changePenIcon(key: string, value: string) {
        if (config !== null) {
            config.set(key, value);
            // Update the grid
            if (grid !== null) {
                grid.changePenIcon(config.penIcon);
            }
        }
    }

    /**
     * ChangePenColor
     * Function to change the pen color
     * @param key - The key of the config to change
     * @param value - The new value
     * @returns void
     */
    function changePenColor(key: string, value: string) {
        if (config !== null) {
            config.set(key, value);
            // Update the grid
            if (grid !== null) {
                grid.changePenColor(config.penColor);
            }
        }
    }

    return (
        <>
            <div className="m-2 rounded-lg shadow-lg border-2 border-gray-200 dark:border-gray-700 flex flex-row items-center justify-between p-2" id="configCardDiv">

                {/* Start */}
                <div>
                    {/* Dimension config */}
                    <Button icon="fas fa-ruler" className="p-button-rounded p-button-text me-2" tooltip="Dimensions du terrain de jeu" tooltipOptions={{ position: 'top' }}
                        onClick={(e) => {
                            opDim.current?.toggle(e)
                        }
                        }
                    />
                    <OverlayPanel ref={opDim} id="overlay_panel_dim" showCloseIcon={true}>
                        <div className="p-0">
                            <h5><b>Dimensions du terrain de jeu</b></h5>
                            <div className="flex flex-row items-center justify-center mt-2">
                                <Slider value={dim} onChange={handleSliderChange} min={2} max={20} className="w-full md:w-2/4" />
                                <InputText value={dim.toString()} onChange={handleInputChange} className="ms-3 w-full md:w-2/4 text-center" />
                            </div>
                        </div>
                    </OverlayPanel>

                    {/* Start position config */}
                    <Button icon="fas fa-flag" className="p-button-rounded p-button-text me-2" tooltip="Position et orientation de départ" tooltipOptions={{ position: 'top' }} onClick={(e) => {
                        opPos.current?.toggle(e)
                    }} />

                    <OverlayPanel ref={opPos} id="overlay_panel_pos" showCloseIcon={true}>
                        <div className="p-0">
                            <h5><b>Position de départ</b></h5>
                            <div className="flex flex-col items-center justify-center mt-2">

                                <div className="flex flex-col items-center justify-center">
                                    <span className='mb-2'>Horizontale</span>

                                    <InputNumber value={posX} onValueChange={(e) => setPosX(e.value ?? 0)} showButtons className='ps-3 mb-3'
                                        size={8}
                                        min={0}
                                        max={dim - 1}
                                        onChange={handlePenStartPosXChange}
                                    />

                                    <span className='mb-2'>Verticale</span>

                                    <InputNumber value={posY} onValueChange={(e) => setPosY(e.value ?? 0)} showButtons className='ps-3'
                                        size={8}
                                        min={0}
                                        max={dim - 1}
                                        onChange={handlePenStartPosYChange}
                                    />
                                </div>
                            </div>
                            <br />

                            <h5 className={"mt-2"}><b>Orientation de départ</b></h5>
                            {/* 4 choices : up, down, right, left */}
                            <div className="flex flex-row items-center justify-center mt-2">
                                <Button
                                    severity={`${angle === 0 ? 'success' : 'secondary'}`}
                                    icon="fas fa-arrow-right" className='p-button-rounded p-button-text me-2' tooltip="Haut" tooltipOptions={{ position: 'top' }} onClick={() => {
                                        setAngle(0);
                                        changePenStartAngle('startAngle', 0);
                                    }} />
                                <Button
                                    severity={`${angle === 180 ? 'success' : 'secondary'}`}
                                    icon="fas fa-arrow-left" className="p-button-rounded p-button-text me-2" tooltip="Bas" tooltipOptions={{ position: 'top' }} onClick={() => {
                                        setAngle(180);
                                        changePenStartAngle('startAngle', 180);
                                    }} />
                                <Button
                                    severity={`${angle === 90 ? 'success' : 'secondary'}`}
                                    icon="fas fa-arrow-down" className="p-button-rounded p-button-text me-2" tooltip="Droite" tooltipOptions={{ position: 'top' }} onClick={() => {
                                        setAngle(90);
                                        changePenStartAngle('startAngle', 90);
                                    }} />
                                <Button
                                    severity={`${angle === 270 ? 'success' : 'secondary'}`}
                                    icon="fas fa-arrow-up" className="p-button-rounded p-button-text" tooltip="Gauche" tooltipOptions={{ position: 'top' }} onClick={() => {
                                        setAngle(270);
                                        changePenStartAngle('startAngle', 270);
                                    }} />
                            </div>
                        </div>
                    </OverlayPanel>

                    {/* Pen style selector */}
                    <Button icon="fas fa-pen" className="p-button-rounded p-button-text me-2" tooltip="Style du crayon" tooltipOptions={{ position: 'top' }}
                        onClick={(e) => {
                            opPen.current?.toggle(e)
                        }} />
                    <OverlayPanel ref={opPen} id="overlay_panel_pen" showCloseIcon={true}>
                        <div className="p-0">
                            <h5><b>Style du crayon</b></h5>

                            {/* Pen icon */}
                            <div className="flex flex-row items-center justify-center mt-2">
                                <Button
                                    severity={`${penIcon === "arrow" ? 'success' : 'secondary'}`}
                                    icon="fas fa-arrow-right" className="p-button-rounded p-button-text me-2" tooltipOptions={{ position: 'top' }} onClick={() => {
                                        setPenIcon("arrow");
                                        changePenIcon("penIcon", "arrow");
                                    }} />

                                <Button
                                    severity={`${penIcon === "car" ? 'success' : 'secondary'}`}
                                    icon="fas fa-car-side" className="p-button-rounded p-button-text" tooltipOptions={{ position: 'top' }} onClick={() => {
                                        setPenIcon("car");
                                        changePenIcon("penIcon", "car");
                                    }} />

                            </div>

                            {/* Pen color */}
                            <div className="flex flex-row items-center justify-center mt-2">


                                <ColorPicker value={penColor} onChange={(e) => {
                                    setPenColor(e.value?.toString() || "ff0000");
                                    changePenColor("penColor", e.value?.toString() || "ff0000")
                                }} />



                            </div>
                        </div>
                    </OverlayPanel>
                </div>

                {/* End */}
                <div className="flex items-center justify-center">
                    {/* Share */}
                    <Button icon="fas fa-share-alt" className="p-button-rounded p-button-text me-2" tooltip="Partager" tooltipOptions={{ position: 'top' }} />
                    {/* Screen capture */}
                    <Button icon="fas fa-camera" className="p-button-rounded p-button-text me-2" tooltip="Capture d'écran" tooltipOptions={{ position: 'top' }}
                        onClick={() => {
                            console.log('Capture element');
                            setdlgCaptureVisible(true);
                        }} />
                </div>

            </div>
            <ScreenCaptureModal visible={dlgCaptureVisible} setVisible={setdlgCaptureVisible} />
        </>
    );
}

export { ConfigDiv };