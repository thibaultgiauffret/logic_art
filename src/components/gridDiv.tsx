/**
 * gridDiv.tsx
 * This file defines the React component `GridDiv` which displays a grid interface.
 * It applies a dark or light theme to the grid instance based on the `isDarkMode` prop.
 * The component uses a `gridDiv` ref to attach the grid interface to a `div` element.
 * @prop none - This component does not accept any props but uses the context to access the values needed.
 * @export GridDiv
 * @export ObjectiveGridDiv
 */

import { useEffect } from 'react';


import { useAppContext, AppContextType } from '../context';

function GridDiv() {

    console.log('GridDiv render');

    // Init context
    const { isDarkMode, gridDiv, grid, mode } = useAppContext() as AppContextType;

    useEffect(() => {
        if (grid) {
            grid.changeTheme(isDarkMode);
        }
    }, [isDarkMode, grid]); // Dependency on isDarkMode and grid to change theme

    console.log('GridDiv mode:', mode);

    let heightClass = '';
    if (mode === "objective") {
        heightClass = 'h-[calc(50vh-10px)] md:h-[calc(50vh-52px)]';
    } else if (mode === "config") {
        heightClass = 'h-[calc(100vh-10px)] md:h-[calc(100vh-174px)]';
    } else {
        heightClass = 'h-[calc(100vh-96px)]';
    }

    return (
        <div className={`m-2 p-0 rounded-lg shadow-lg border-2 border-gray-200 dark:border-gray-700 flex flex-col items-center ${heightClass}`} id="gridCardDiv">
            <span className="mt-2">Terrain de jeu</span>
            <div className="flex flex-row items-center align-cente h-full">
                <div id="gridDiv" ref={gridDiv}>
                </div>
            </div>
        </div>
    );
}

function ObjectiveGridDiv() {

    console.log('ObjectiveGridDiv render');

    // Init context
    const { isDarkMode, gridDiv, grid } = useAppContext() as AppContextType;

    useEffect(() => {
        if (grid) {
            grid.changeTheme(isDarkMode);
        }
    }, [isDarkMode, grid]); // Dependency on isDarkMode and grid to change theme

    return (
        <div className="m-2 p-0 rounded-lg shadow-lg border-2 border-gray-200 dark:border-gray-700 flex flex-col items-center h-[calc(50vh-10px)] md:h-[calc(50vh-52px)]" id="objectiveGridCardDiv">
            <span className="mt-2">Objectif</span>
            <div id="objectiveGridDiv" ref={gridDiv} className="m-2"></div>
        </div>
    );
}

export { GridDiv, ObjectiveGridDiv };