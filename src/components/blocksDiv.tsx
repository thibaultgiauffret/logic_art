/**
 * BlocksDiv.tsx
 * This file defines the React component `BlocksDiv` which displays a Blockly workspace.
 * It applies a dark or light theme to the Blockly instance based on the `isDarkMode` prop.
 * The component uses a `blocksDiv` ref to attach the Blockly workspace to a `div` element.
 * @prop none - This component does not accept any props but uses the context to access the values needed.
 * @export BlocksDiv
 */

import { useEffect } from 'react';
import { useAppContext, AppContextType } from '../context';

function BlocksDiv () {

  console.log('BlocksDiv render');

  // Init context
  const { isDarkMode, blocks, blocksDiv } = useAppContext() as AppContextType;

  // Apply dark mode theme
  useEffect(() => {
    if (blocks) {
      blocks.changeTheme(isDarkMode);
    }
  }, [isDarkMode, blocks]); // Dependency on isDarkMode and blocks to change theme

  return (
    <div className="m-2 p-0 rounded-lg shadow-lg border-2 border-gray-200 dark:border-gray-700"id="blocksCardDiv">
      <div id="blocksDiv" ref={blocksDiv} style={{ height: 'calc(100vh - 100px)', width: '100%' }}></div>
    </div>
  );
};

export default BlocksDiv;