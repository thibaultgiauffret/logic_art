/**
 * screenCapture.tsx
 * This file defines the React component `ScreenCaptureModal` which displays the screen capture modal (user can choose the element to capture : blockly, grid, or both).
 * @prop {boolean} visible - The visibility of the modal.
 * @prop {function} setVisible - The function to set the visibility of the modal.
 * @exports ScreenCaptureModal
 */

import { useRef, useState } from 'react';
import { Button } from 'primereact/button';
import { Dialog } from 'primereact/dialog';
import { Dropdown } from 'primereact/dropdown';
import { Toast } from 'primereact/toast';

import screenCapture from '../../ts/functions/screenCapture';

interface ScreenCaptureModalProps {
    visible: boolean;
    setVisible: (visible: boolean) => void;
}

function ScreenCaptureModal({ visible, setVisible }: ScreenCaptureModalProps) {
    const [element, setElement] = useState<string>('blockly');
    const toast = useRef<Toast>(null);

    const capture = () => {
        screenCapture(element).then(() => {
            console.log('Screen captured');
            toast.current?.show({ severity: 'success', summary: 'Capture effectuée avec succès' });
            setVisible(false);
        }).catch((error) => {
            console.error('Erreur lors de la capture:', error);
            toast.current?.show({ severity: 'error', summary: 'Erreur lors de la capture', detail: error.toString() });
            setVisible(false);
        })
    }

    const dialogFooter = (
        <div>
            <Button icon="pi pi-times" className="p-button-text m-1" onClick={() => setVisible(false)} label="Annuler" severity="secondary" />
            <Button icon="pi pi-check" className="m-1" onClick={capture} label="Capturer"/>
        </div>
    );

    return (
        <div>
            <Toast ref={toast} />
            <Dialog visible={visible} style={{ width: '450px' }} footer={dialogFooter} onHide={() => setVisible(false)} 
             header="Capture d'écran" modal>
                <div className="flex flex-col items-center justify-center">
                    <Dropdown value={element} options={[{ label: 'Blocs', value: 'blocksDiv' }, { label: 'Grille', value: 'gridDiv' }, { label: 'Page entière', value: 'content' }]} onChange={(e) => setElement(e.value)} placeholder={'Sélectionner une zone à capturer'} />
                </div>
            </Dialog>
        </div>
    );
}

export default ScreenCaptureModal;