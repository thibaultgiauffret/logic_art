/**
 * topbar.tsx
 * This file defines the React component `Topbar` which displays a top navigation bar.
 * It includes buttons and switches for interacting with the Blockly workspace and grid interface.
 * The component uses props to manage dark mode, and to interact with instances of `Blocks` and `Grid`.
 * @prop none - This component does not accept any props but uses the context to access the values needed.
 * @export Topbar
 */

import { useState, useRef } from 'react';

import { Button } from 'primereact/button';
import { Menubar } from 'primereact/menubar';
import { InputSwitch } from "primereact/inputswitch";
import { Toast } from 'primereact/toast';

// Load the IFrame integration API (to send data to the IFrame on code run)
import { sendAssignmentDataToIFrame } from '../ts/api/iframeIntegration';

// Load the context
import { useAppContext, AppContextType } from '../context';

// Load the functions to save and load data
import { saveToLocalStorage, saveToFile } from '../ts/functions/save';
import { loadFromFile } from '../ts/functions/load';

function Topbar() {

  console.log('Topbar render');

  // Init context
  const { isDarkMode, setIsDarkMode, blocks, grid, config } = useAppContext() as AppContextType;

  // Init toast
  const toast = useRef<Toast>(null);

  // Switch state for step by step mode (not functional yet)
  const [checked, setChecked] = useState(false);

  // Handle click on the run button
  const handleRunClick = async () => {

    // Send the assignment data to the IFrame
    sendAssignmentDataToIFrame();

    const code = blocks?.getCode();
    const pen = grid !== null ? grid.pen : null;
    const result = await pen?.executeCode(code!, false);
    // Check if pen is out of bounds
    if (!result?.status) {
      toast.current?.show({ severity: 'error', summary: 'Attention', detail: result?.message });
    }
  }

  const handleDownloadClick = async () => {
    console.log('Downloading file');
    const result = await saveToFile(config!, grid!, blocks!);
    if (result) {
      toast.current?.show({ severity: 'success', summary: 'Téléchargement du fichier', detail: 'Téléchargement réussi.' });
    } else {
      toast.current?.show({ severity: 'error', summary: 'Téléchargement du fichier', detail: 'Erreur lors du téléchargement.' });
    }
  }

  const handleUploadClick = async () => {
    console.log('Uploading file');
    // Show file dialog
    const input = document.createElement('input');
    input.type = 'file';
    input.accept = '.json';
    input.onchange = (e) => {
      const file = (e.target as HTMLInputElement).files?.[0];
      if (file) {
        const reader = new FileReader();
        reader.onload = async (e) => {
          const content = e.target?.result;
          if (content) {
            loadFromFile(content as string, config!, grid!, blocks!);
            toast.current?.show({ severity: 'success', summary: 'Chargement du fichier', detail: 'Chargement réussi.' });
            // Run the code
            const code = blocks?.getCode();
            const pen = grid !== null ? grid.pen : null;
            try {
              const result = await pen?.executeCode(code!, false);
              if (!result?.status) {
                if (toast.current !== null) {
                  toast.current!.show({ severity: 'error', summary: 'Attention', detail: result?.message });
                }
              }
            } catch (e) {
              console.error('Error executing code : ', e);
            }
          }
        }
        reader.readAsText(file);

      }
    }
    input.click();
  }

  // Build top bar elements
  // Logo
  const start = (
    <div className="flex items-center justify-center">
      {/* Logo */}
      <img alt="logo" src="./logic_art.svg" height="10" className="mx-3 mr-4 logo"></img>
      {/* Save */}
      <Button icon="fas fa-save" className="p-button-rounded p-button-text"
        rounded text severity="secondary"
        style={{ aspectRatio: '1/1', backgroundColor: 'transparent', borderColor: 'transparent', color: 'inherit' }}
        tooltip="Sauvegarder" tooltipOptions={{ position: 'top' }}
        onClick={() => {
          console.log('Saving to local storage');
          const state = saveToLocalStorage(config!, grid!, blocks!);
          if (state.status === "success") {
            toast.current?.show({ severity: 'success', summary: 'Sauvegarde dans le navigateur', detail: 'Sauvegarde réussie.' });
          } else if (state.status === "error") {
            toast.current?.show({ severity: 'error', summary: 'Sauvegarde dans le navigateur', detail: 'Erreur lors de la sauvegarde.' });
          }
        }} />
      {/* Download */}
      <Button id="downloadButton"
        icon="fas fa-download"
        className="p-button-rounded p-button-text ms-2"
        rounded text severity="secondary"
        style={{ aspectRatio: '1/1', backgroundColor: 'transparent', borderColor: 'transparent', color: 'inherit' }}
        tooltip="Télécharger"
        tooltipOptions={{ position: 'top' }}
        onClick={handleDownloadClick}
      />
      {/* Upload */}
      <Button id="uploadButton"
        icon="fas fa-upload"
        className="p-button-rounded p-button-text ms-2"
        rounded text severity="secondary"
        style={{ aspectRatio: '1/1', backgroundColor: 'transparent', borderColor: 'transparent', color: 'inherit' }}
        tooltip="Charger"
        tooltipOptions={{ position: 'top' }}
        onClick={handleUploadClick}
      />
    </div>
  );

  // End section with theme switch, step by step switch, and play button
  const end = (
    <div className="flex items-center justify-center">
      {/* Theme switch button */}
      <Button
        id="themeButton"
        className="!rounded-full me-2"
        rounded text severity="secondary"
        style={{ aspectRatio: '1/1', backgroundColor: 'transparent', borderColor: 'transparent', color: 'inherit' }}
        icon={isDarkMode ? "fas fa-sun" : "fas fa-moon"}
        onClick={() => setIsDarkMode(!isDarkMode)}
      />

      {/* Step by step switch with label */}
      <InputSwitch checked={checked} onChange={(e) => setChecked(e.value)} className="mr-3" />
      <span className="mr-3">Mode pas à pas</span>

      {/* Play button */}
      <Button
        id="runButton"
        icon="fas fa-play"
        className="py-2 px-4 mr-3 !rounded-full"
        style={{ aspectRatio: '1/1' }}
        severity='success'
        onClick={handleRunClick}
      />
    </div>
  );

  return (
    <>
      {/* TODO : duplicate with toast in App component */}
      <Toast ref={toast} />
      <div className='topbarContainer p-2'>
        <Menubar className="topbar shadow-lg" start={start} end={end} />
      </div>
    </>
  );

}

export default Topbar;