/**
 * context.tsx
 * This file defines the React context `AppContext` which provides the values needed by the components.
 * It includes the context provider `AppContextProvider` which initializes the context values.
 * The context values include the `isFirstRender`, `isDarkMode`, `blocks`, `grid`, `objectiveGrid`, `config`, `mode`, and `gui`.
 * @export AppContextProvider
 */


import { createContext, useState, useEffect, ReactNode, useRef, useContext } from 'react';
import GUI from './ts/gui';
import Blocks from './ts/blocks';
import Config from './ts/config';
import Grid from './ts/grid';

export interface AppContextType {
    isFirstRender: boolean;
    setIsFirstRender: (value: boolean) => void;
    isDarkMode: boolean;
    setIsDarkMode: (value: boolean) => void;
    blocks: Blocks | null;
    grid: Grid | null;
    objectiveGrid: Grid | null;
    config: Config | null;
    mode: string | null;
    gui: GUI | null;
    blocksDiv: React.RefObject<HTMLDivElement>;
    gridDiv: React.RefObject<HTMLDivElement>;
    objectiveGridDiv: React.RefObject<HTMLDivElement>;
}

// Create context
const AppContext = createContext<AppContextType | null>(null);

interface AppContextProviderProps {
    children: ReactNode;
}

// Create context provider
export const AppContextProvider: React.FC<AppContextProviderProps> = ({ children }) => {
    const blocksDiv = useRef<HTMLDivElement>(null);
    const [blocks, setBlocks] = useState<Blocks | null>(null);

    const gridDiv = useRef<HTMLDivElement>(null);
    const [grid, setGrid] = useState<Grid | null>(null);

    const objectiveGridDiv = useRef<HTMLDivElement>(null);
    const [objectiveGrid, setObjectiveGrid] = useState<Grid | null>(null);

    const [config, setConfig] = useState<Config | null>(null);

    const [mode, setMode] = useState<string | null>(null);

    const gui = useRef<GUI | null>(null);

    // Create a state to check if it's the first render
    const [isFirstRender, setIsFirstRender] = useState(true);


    // Initialize context values
    useEffect(() => {
        if (blocksDiv.current && gridDiv.current) {
            gui.current = new GUI(blocksDiv.current!, gridDiv.current!);
            gui.current.initGUI();
            setBlocks(gui.current.blocks);
            setGrid(gui.current.grid);
            setMode(gui.current.mode);
        }
    }, [blocksDiv, gridDiv]);

    useEffect(() => {
        if (objectiveGridDiv.current !== null && gui.current !== null) {
            gui.current.addObjectiveGrid(objectiveGridDiv.current!);
            gui.current.objectiveGrid?.init();
            if (gui.current.objectiveGrid !== undefined) {
                setObjectiveGrid(gui.current.objectiveGrid);
            }
        }
    }, [objectiveGridDiv]);

    useEffect(() => {
        const configInstance = new Config();
        configInstance.init();
        setConfig(configInstance);
    }, []);


    /**
     * Handle dark mode
     */
    const [isDarkMode, setIsDarkMode] = useState(false);

    useEffect(() => {
        console.log('isDarkMode changed : ', isDarkMode);
        const themeLink = document.getElementById('theme-link') as HTMLLinkElement;
        if (themeLink) {
            themeLink.href = isDarkMode
                ? '/node_modules/primereact/resources/themes/lara-dark-blue/theme.css'
                : '/node_modules/primereact/resources/themes/lara-light-blue/theme.css';
        }

        // Add data-mode attribute to the body
        document.body.setAttribute('data-mode', isDarkMode ? 'dark' : 'light');
    }, [isDarkMode]);

    return (
        <AppContext.Provider value={{ blocks, grid, objectiveGrid, config, mode, gui: gui.current, blocksDiv, gridDiv, objectiveGridDiv, isDarkMode, setIsDarkMode, isFirstRender, setIsFirstRender }}>
            {children}
        </AppContext.Provider>
    );
};

export const useAppContext = () => {
    return useContext(AppContext);
}