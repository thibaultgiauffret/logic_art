import React from 'react';
import App from './app';
import { AppContextProvider } from './context';

const AppWithProvider: React.FC = () => {

  return (
    <AppContextProvider>
      <App />
    </AppContextProvider>
  );
};

export default AppWithProvider;