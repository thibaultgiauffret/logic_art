// Interfaces for Iframe Integration API
import Config  from "../ts/config";
export interface AssignmentData {
    // TODO
    result: string;
}

export interface StudentAssignment {
    // TODO
    result: string;
}

export interface ActivityParams {
    mode: 'create' | 'assignment' | 'review' | 'view',
    activity: Config,
    workflow: 'current' | 'finished' | 'corrected',
    studentAssignment: StudentAssignment,
    assignmentData: AssignmentData
}