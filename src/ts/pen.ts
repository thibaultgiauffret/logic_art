/**
 * pen.ts
 * This file defines the `Pen` class which manages the drawing functionalities in the application.
 * It includes methods for initializing the pen, drawing on the canvas, and handling user inputs.
 * @prop canvasSize: number - The size of the canvas.
 * @prop svg: SVGSVGElement | null - The SVG element of the canvas.
 * @prop svgUserLayer: SVGSVGElement | null - The SVG element of the user layer.
 * @prop dim: number - The dimension of the grid.
 * @prop penObjStartPos: { x: number, y: number } - The starting position of the pen.
 * @prop penObjStartAngle: number - The starting angle of the pen.
 * @prop penIcon: string - The icon of the pen.
 * @prop penColor: string - The color of the pen.
 * @export default Pen
 */
import arrow from '@/icons/arrow.svg?raw';
import car from '@/icons/car.svg?raw';


// Types
interface Line {
  type: string;
  times?: number;
  branch?: Line[];
}

export default class Pen {
  private _canvas: SVGSVGElement | null
  private _layer: SVGSVGElement | null
  private _dim: number
  private _canvasSize: number
  private _penObj: SVGSVGElement
  private _iconElement: SVGSVGElement | undefined;
  private _paths: NodeListOf<SVGPathElement> | undefined;
  private _icons: { [key: string]: string } = {
    arrow: arrow,
    car: car,
  };

  private _penObjStartPos: { x: number, y: number }
  private _penObjPos: { x: number, y: number }
  private _penObjAngle: number
  private _penObjStartAngle: number
  private _penIcon: string
  private _penColor: string
  private _penSize: number
  private _stepCounter: number = 0

  public constructor(canvasSize: number, svg: SVGSVGElement | null, svgUserLayer: SVGSVGElement | null, dim: number, penObjStartPos: { x: number, y: number }, penObjStartAngle: number, penIcon: string, penColor: string) {
    this._canvas = svg
    this._layer = svgUserLayer

    this._dim = dim
    this._canvasSize = canvasSize
    this._penSize = 0.9 * this._canvasSize / this._dim
    this._penObjStartPos = penObjStartPos
    this._penObjPos = { x: 0, y: 0 }
    this._penObjAngle = 0
    this._penObjStartAngle = penObjStartAngle
    this._penIcon = penIcon
    this._penColor = penColor
    this._penObj = this._createPen()
    this._stepCounter = 0
  }

  /**
     * Add the pen to the canvas
     */
  public addPen() {
    if (!this._canvas) {
      console.error('Canvas element is not initialized');
      return;
    }

    this._layer!.appendChild(this._penObj)
    console.log('Pen added:', this._penObj);
  }

  /**
   * Save method
   * This method is called when the user clicks the save button.
   */
  public savePen(): object {
    console.log('Save pen')
    return {
      penObjPos: this._penObjPos,
      penObjAngle: this._penObjAngle,
      penSize: this._penSize,
      penIcon: this._penIcon,
      penColor: this._penColor
    }
  }

  /**
   * Change the pen position
   */
  public changePenPosition(x: number, y: number) {
    const rectSize = this._canvasSize / this._dim
    this._penObjPos = { x: x, y: y }
    this._penObj.setAttribute('x', (x * rectSize).toString())
    this._penObj.setAttribute('y', (y * rectSize).toString())
  }

  /**
   * Change the pen start position
   * @param x
   * @param y
   * @returns void
   **/
  public changePenStartPos(x: number, y: number) {
    this.cleanCanvas()
    this._penObjStartPos = { x: x, y: y }
    this.resetPen()
  }

  /**
   * Change the pen start angle
   * @param angle
   * @returns void
   */
  public changePenStartAngle(angle: number) {
    this.cleanCanvas()
    this._penObjStartAngle = angle
    this.resetPen()
  }

  /**
   * Change the pen icon
   * @param icon
   * @returns void
   */
  public changePenIcon(icon: string) {
    this._penIcon = icon;
    this._updateIcon();
  }

  /**
   * Change the pen color
   * @param color
   * @returns void
   */
  public changePenColor(color: string) {
    this._penColor = color;
    this._updateColor();
  }

  /**
 * Create the pen
 * @returns SVGSVGElement
 */
  private _createPen(): SVGSVGElement {
    console.log('Create pen');
    const rectSize = this._canvasSize / this._dim;
    const x = this._penObjStartPos.x;
    const y = this._penObjStartPos.y;

    const penSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    penSVG.setAttribute('x', x.toString());
    penSVG.setAttribute('y', y.toString());
    penSVG.setAttribute('width', rectSize.toString());
    penSVG.setAttribute('height', rectSize.toString());

    const penContentSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    this._iconElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    this._iconElement.setAttribute('viewBox', '0 0 24 24');
    this._iconElement.setAttribute('x', (0.05 * rectSize).toString());
    this._iconElement.setAttribute('y', (0.05 * rectSize).toString());
    this._iconElement.setAttribute('width', (0.9 * rectSize).toString());
    this._iconElement.setAttribute('height', (0.9 * rectSize).toString());

    penContentSVG.appendChild(this._iconElement);
    penContentSVG.setAttribute('id', 'penContent');
    penSVG.appendChild(penContentSVG);

    this._updateIcon();

    return penSVG;
  }

  /**
   * Update the icon
   * @returns void
   * Find the icon in the icons object and update the icon element with the new icon
   */
  private _updateIcon() {
    const svgContent = this._icons[this._penIcon];
    if (svgContent) {
      if (this._iconElement) {
        this._iconElement.innerHTML = svgContent;
        this._paths = this._iconElement.querySelectorAll('path');
        this._updateColor();
      } else {
        console.error('Icon element is not initialized');
      }
    } else {
      console.error('Icon not found:', this._penIcon);
    }
  }

  /**
   * Update the color of the icon
   * @returns void
   * Find each path element in the icon and set their fill to the pen color
   */
  private _updateColor() {
    if (this._paths) {
      this._paths.forEach(path => {
        path.setAttribute('fill', `#${this._penColor}`);
      });
    }
  }

  /**
   * Reset the pen position
   * @returns void
   */
  public resetPen() {
    this.changePenPosition(this._penObjStartPos.x, this._penObjStartPos.y)
    this._penObjAngle = this._penObjStartAngle
    this.rotatePen()
    this._penSize = 0.9 * this._canvasSize / this._dim
  }

  /**
   * Clean the user layer of the canvas
   * @returns void
   * Find each rect element in the layer and set their fill to none
   */
  public cleanCanvas() {
    const rects = this._layer!.querySelectorAll('rect')
    for (const rect of rects) {
      rect.setAttribute('fill', 'none')
    }
  }

  /**
   * Clean the pen
   * @returns void
   * Remove the pen obj
   */
  public cleanPen() {
    this._layer!.removeChild(this._penObj)
  }

  /**
     * Rotate the pen
     * @returns void
     */
  private rotatePen() {
    const pen = this._penObj.querySelector('#penContent')!
    // Rotate the pen with transform, using its center as the rotation point
    pen.setAttribute('transform', `rotate(${this._penObjAngle}, ${this._canvasSize / this._dim / 2}, ${this._canvasSize / this._dim / 2})`)

  }

  /**
     * Change the color of the rectangle at position x, y in the user layer
     * @param x
     * @param y
     * @param color
     * @returns void
     */
  private _changeColor(x: number, y: number, color: string) {
    const rectSize = this._canvasSize / this._dim
    const rect = this._layer!.querySelector(`rect[x="${Math.round((x * rectSize) * 10000) / 10000}"][y="${Math.round((y * rectSize) * 10000) / 10000}"]`)!
    console.log('x :', (x * rectSize).toString(), 'y :', y * rectSize)
    rect.setAttribute('fill', color)
  }

  /**
     * Execute the code
     * @param code
     * @returns void
     */
  public async executeCode(code: string, stepByStep: boolean) {
    // Next lines are not clean... Better way to do it ?
    code = '[' + code + ']'
    // Replace all }{ by },{ to have a valid JSON
    code = code.replace(/}\s*{/g, '},{')
    // Parse the code to JSON
    const parsedCode = JSON.parse(code)
    // Remove the first element of the array (the first element is always an empty object)
    parsedCode.shift()
    // Custom eval function
    if (stepByStep) {
      this._eval(parsedCode[this._stepCounter], true)
      this._stepCounter += 1
      if (this._stepCounter > parsedCode.length) {
        this._stepCounter = 0
        this.resetPen()
        this.cleanCanvas()
      }
    } else {
      // Reset the pen position
      this.resetPen()
      this.cleanCanvas()
      for (const line of parsedCode) {
        // Check boundaries
        const go = await this._eval(line)
        if (go && !go.status) {
          return { status: false, message: go.message }
        }
      }
      return { status: true }
    }
  }

  /**
   * Check if the pen is out of bounds
   * @returns boolean
   */
  private checkBoundaries(x: number, y: number) {
    // Fix errors due to floating point precision (round to 4 decimals)
    x = Math.round(x * 10000) / 10000
    y = Math.round(y * 10000) / 10000
    if (x < 0 || x > this._dim - 1 || y < 0 || y > this._dim - 1) {
      // Set the pen position to start position
      this.changePenPosition(this._penObjStartPos.x, this._penObjStartPos.y)
      // this._penObjAngle = 
      this.rotatePen()
      return false
    } else {
      return true
    }
  }

  /**
     * Custom eval function
     * @param code
     * @param stepByStep
     * @returns { status: boolean, message?: string } | void
     * Note : Not using eval() because of safety concerns...
     * First try with a switch case. Slow with loops, but safer. Will improve later.
     */
  private async _eval(line: Line, stepByStep: boolean = false): Promise<{ status: boolean, message?: string } | void> {

    if (line === undefined) {
      return
    }
    switch (line.type) {
      case 'forward':
        try {
          this.forward()
          return { status: true }
        } catch (e: any) {
          if (e.message === 'Out of boundaries') {
            return { status: false, message: 'Le stylo est sorti du plateau. Veuillez vérifier votre code.' }
          }
        }
        break
      case 'backward':
        try {
          this.backward()
          return { status: true }
        } catch (e: any) {
          if (e.message === 'Out of boundaries') {
            return { status: false, message: 'Le stylo est sorti du plateau. Veuillez vérifier votre code.' }
          }
        }
        break
      case 'turnRight':
        this.turnRight()
        return { status: true }
        break
      case 'turnLeft':
        this.turnLeft()
        return { status: true }
        break
      case 'drawRed':
        this.drawRed()
        return { status: true }
        break
      case 'drawGreen':
        this.drawGreen()
        return { status: true }
        break
      case 'drawBlue':
        this.drawBlue()
        return { status: true }
        break
      case 'drawYellow':
        this.drawYellow()
        return { status: true }
        break
      case 'repeat':
        for (let i = 0; i < line.times!; i++) {
          for (const block of line.branch!) {

            const go = await this._eval(block, stepByStep)
            if (go && !go.status) {
              return { status: false, message: go.message }
            }

            // Not clean... Better way to do it ?
            if (stepByStep) {
              // Add a delay of 1s
              await new Promise(resolve => setTimeout(resolve, 1000));
            }
          }
        }
        return { status: true }
        break;
    }
  }

  private forward() {
    // Calculate the new position
    const newX = this._penObjPos.x + Math.cos(this._penObjAngle * (Math.PI / 180));
    const newY = this._penObjPos.y + Math.sin(this._penObjAngle * (Math.PI / 180));
    // Check boundaries
    if (!this.checkBoundaries(newX, newY)) {
      throw new Error('Out of boundaries');
    }
    // Update the position of the pen
    this._penObjPos.x = newX;
    this._penObjPos.y = newY;
    // Move the pen
    this.movePen();
  }


  private backward() {
    // Calculate the new position
    const newX = this._penObjPos.x - Math.cos(this._penObjAngle * (Math.PI / 180));
    const newY = this._penObjPos.y - Math.sin(this._penObjAngle * (Math.PI / 180));
    // Check boundaries
    if (!this.checkBoundaries(newX, newY)) {
      throw new Error('Out of boundaries');
    }
    // Update the position of the pen
    this._penObjPos.x = newX;
    this._penObjPos.y = newY;
    // Move the pen
    this.movePen();
  }

  // Move the pen
  private movePen() {
    this.changePenPosition(this._penObjPos.x, this._penObjPos.y);
  }

  private turnRight() {
    // Turn the pen right
    this._penObjAngle = (this._penObjAngle + 90) % 360
    // Rotate the pen
    this.rotatePen()
  }

  private turnLeft() {
    // Turn the pen left
    this._penObjAngle = (this._penObjAngle + 270) % 360
    // Rotate the pen
    this.rotatePen()
  }

  private drawRed() {
    // Get the position of the pen
    const x = this._penObjPos.x;
    const y = this._penObjPos.y;
    // Change the color of underneath the pen to red
    this._changeColor(x, y, 'red');
  }

  private drawGreen() {
    // Get the position of the pen
    const x = this._penObjPos.x;
    const y = this._penObjPos.y;
    // Change the color of underneath the pen to green
    this._changeColor(x, y, 'green');
  }

  private drawBlue() {
    // Get the position of the pen
    const x = this._penObjPos.x
    const y = this._penObjPos.y
    // Change the color of underneath the pen to blue
    this._changeColor(x, y, 'blue')
  }

  private drawYellow() {
    // Get the position of the pen
    const x = this._penObjPos.x
    const y = this._penObjPos.y
    // Change the color of underneath the pen to yellow
    this._changeColor(x, y, 'yellow')
  }
}
