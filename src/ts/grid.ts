/**
 * grid.ts
 * This file defines the `Grid` class which manages the grid interface in the application.
 * It includes methods for initializing the grid, updating its content, and handling user interactions.
 * @prop gridDiv: HTMLElement - The div element containing the grid.
 * @prop objective: boolean - A flag indicating whether the grid is the objective grid.
 * @export default Grid
 */

import Pen from './pen'

export default class Grid {
  private _canvas: HTMLElement
  private _canvasSize: number
  private _dim: number
  private _startPosX: number
  private _startPosY: number
  private _startAngle: number
  private _penIcon: string
  private _penColor: string
  public pen: Pen | null
  public svg: SVGSVGElement | null
  public svgUserLayer: SVGSVGElement | null
  private _div: HTMLElement
  private _darkMode: boolean
  private _objective: boolean

  public constructor(gridDiv: HTMLElement, objective: boolean = false) {
    this._dim = 10
    this._startPosX = 0
    this._startPosY = 0
    this._startAngle = 0
    this._penIcon = 'arrow'
    this._penColor = 'ff0000'
    this._canvasSize = 400
    this.pen = null
    this.svg = null
    this.svgUserLayer = null
    this._div = gridDiv
    this._canvas = document.createElement('div')
    this._canvas.setAttribute('id', 'canvas_' + this._div.id)
    this._canvas.style.position = 'relative'
    this._div.appendChild(this._canvas)
    this._darkMode = false
    this._objective = objective
  }

  /**
   * Initialize the grid
   */
  public async init() {
    console.log('Init grid ' + this._div.id)
    this._setupCanvas()
    window.addEventListener('resize', this.resizeCanvas.bind(this));
  }

  public setDim(dim: number) {
    console.log('Set dim : ', dim)
    this._dim = dim
    // Clear the canvas.
    this.clearCanvas()
    this.clearLayer()
    // Draw the canvas and resize it.
    this.drawCanvas()
    this.drawLayer()
  }

  /**
   * Change the pen start position.
   */
  public changePenStartPos(x: number, y: number) {
    console.log('Change pen start position :', x, y)
    if (this.pen) {
      this._startPosX = x
      this._startPosY = y
      this.pen.changePenStartPos(x, y)
    }
  }

  /**
   * Change the pen start angle.
   */
  public changePenStartAngle(angle: number) {
    console.log('Change pen start angle :', angle)
    if (this.pen) {
      this._startAngle = angle
      this.pen.changePenStartAngle(angle)
    }
  }

  /**
   * Change the pen icon.
   */
  public changePenIcon(icon: string) {
    console.log('Change pen icon :', icon)
    if (this.pen) {
      this._penIcon = icon
      this.pen.changePenIcon(icon)
    }
  }

  /**
   * Change the pen color.
   */
  public changePenColor(color: string) {
    console.log('Change pen color :', color)
    if (this.pen) {
      this._penColor = color
      this.pen.changePenColor(color)
    }
  }

  /**
   * Setup the canvas.
   * @private
   * @returns void
   */
  private _setupCanvas(): void {
    // Clear the canvas.
    this.clearCanvas()
    this.clearLayer()

    // Set the canvas size
    this._canvasSize = 400

    // Set the canvas size
    this._canvas.style.width = `${this._canvasSize}px`
    this._canvas.style.height = `${this._canvasSize}px`

    // Draw the canvas and resize it.
    console.log('Call drawCanvas from _setupCanvas')
    this.drawCanvas()
    this.drawLayer()
  }

  /**
    * Handle window resize event.
    * @public
    */
  public resizeCanvas(): void {
    const parentWidth = this._div.parentElement!.parentElement!.clientWidth;
    const parentHeight = this._div.parentElement!.parentElement!.clientHeight;
    const newWidth = parentWidth - 20;
    const newHeight = parentHeight - 50;

    // Select the minimum between newWidth and newHeight
    const min = Math.min(newWidth, newHeight);

    // Ensure the canvas size does not go above a certain maximum size
    const maxSize = 550; // Set a minimum size for the canvas (change ? remove ?)
    this._canvasSize = Math.min(min, maxSize);

    if (this._canvas) {
      this._canvas.style.width = `${this._canvasSize}px`;
      this._canvas.style.height = `${this._canvasSize}px`;
    }

    // Redraw the canvas after resizing
    console.log('Call drawCanvas from resizeCanvas')
    // this.drawCanvas();
  }

  /**
   * Change the theme of the grid.
   */
  public changeTheme(darkMode: boolean): void {
    this._darkMode = darkMode
    console.log('Call drawCanvas from changeTheme')
    this.clearCanvas()
    this.drawCanvas()
  }

  private calculateGridParameters() {
    const lines = this._dim
    const columns = this._dim
    const bw = this._canvas.clientWidth;
    const bh = this._canvas.clientHeight;
    const px = bw / columns
    const py = bh / lines
    // Find the minimum between px and py.
    const max = Math.max(px, py)
    return { bw, bh, max }
  }

  /**
    * Draw the canvas.
    */
  public drawCanvas() {
    console.log('Draw grid')
    const svgNS = 'http://www.w3.org/2000/svg'
    const svg = document.createElementNS(svgNS, 'svg')
    svg.setAttribute('id', 'canvasSVG_' + this._div.id)
    svg.setAttribute('width', '100%')
    svg.setAttribute('height', '100%')
    svg.setAttribute('viewBox', `0 0 ${this._canvas.clientWidth} ${this._canvas.clientHeight}`)
    svg.setAttribute('preserveAspectRatio', 'xMidYMid meet')

    // Choose the color of the grid
    let borderColor = '#666'
    let fillColor = 'white'
    if (this._darkMode) {
      borderColor = 'white'
      fillColor = '#1f2937'
    }

    const { bw, bh, max } = this.calculateGridParameters()

    for (let x = 0; x < bw; x += max) {
      for (let y = 0; y < bh; y += max) {
        const rect = document.createElementNS(svgNS, 'rect')
        rect.setAttribute('x', (Math.round(x * 10000) / 10000).toString())
        rect.setAttribute('y', (Math.round(y * 10000) / 10000).toString())
        rect.setAttribute('width', max.toString())
        rect.setAttribute('height', max.toString())
        rect.setAttribute('stroke', borderColor)
        rect.setAttribute('fill', fillColor)
        svg.appendChild(rect)
      }
    }

    this._canvas.appendChild(svg)
    this.svg = svg
  }

  public drawLayer() {

    console.log('Draw layer')

    // Add a user layer to the canvas (this layer will be used to draw the pixel art).
    const svgNS = 'http://www.w3.org/2000/svg'
    const svgUserLayer = document.createElementNS(svgNS, 'svg')
    svgUserLayer.setAttribute('id', 'canvasSVGUserLayer_' + this._div.id)
    svgUserLayer.setAttribute('width', '100%')
    svgUserLayer.setAttribute('height', '100%')
    svgUserLayer.setAttribute('viewBox', `0 0 ${this._canvas.clientWidth} ${this._canvas.clientHeight}`)
    svgUserLayer.setAttribute('preserveAspectRatio', 'xMidYMid meet')

    const { bw, bh, max } = this.calculateGridParameters()

    // Add all the rectangles to the user layer (but make them invisible).
    for (let x = 0; x < bw; x += max) {
      for (let y = 0; y < bh; y += max) {
        const rect = document.createElementNS(svgNS, 'rect')
        rect.setAttribute('x', (Math.round(x * 10000) / 10000).toString())
        rect.setAttribute('y', (Math.round(y * 10000) / 10000).toString())
        rect.setAttribute('width', max.toString())
        rect.setAttribute('height', max.toString())
        rect.setAttribute('stroke', 'none')
        rect.setAttribute('fill', 'none')
        svgUserLayer.appendChild(rect)
      }
    }

    // Set the user layer to be on top of the grid layer (z-index and position absolute).
    svgUserLayer.style.position = 'absolute';
    svgUserLayer.style.top = '0';
    svgUserLayer.style.left = '0';
    svgUserLayer.style.zIndex = '2';

    this._canvas.appendChild(svgUserLayer)
    this.svgUserLayer = svgUserLayer

    // Add the pen to the canvas if the grid is not the objective grid.
    if (!this._objective) {
      this.pen = new Pen(this._canvasSize, this.svg, this.svgUserLayer, this._dim, {
        x: this._startPosX,
        y: this._startPosY
      },
      this._startAngle, this._penIcon, this._penColor);
      this.pen.addPen();
    }
  }


  /**
    * Clear the canvas.
    */
  public clearCanvas(): void {
    // Test if there is a canvas element.
    if (this.svg) {
      // Remove this.svg from the canvas.
      this._canvas.removeChild(this.svg)
    }
  }

  public clearLayer(): void {
    // Test if there is a canvas element.
    if (this.svgUserLayer) {
      this.svgUserLayer.innerHTML = ''
    }
  }

  /**
   * Save method
   * This method is called when the user clicks the save button.
   */
  public save(): object {
    console.log('Save grid')
    return {
      dim: this._dim,
      startPosX: this._startPosX,
      startPosY: this._startPosY,
      startAngle: this._startAngle,
      pen: this.pen?.savePen()
    }
  }

  /**
   * Load method
   * This method is called when the user clicks the load button or when the app is initialized.
   */
  public load(data: { dim: number; startPosX: number; startPosY: number; startAngle: number; pen: { penIcon: string; penColor: string } }): void {
    console.log('Load grid')
    this._dim = data.dim
    this._startPosX = data.startPosX
    this._startPosY = data.startPosY
    this._startAngle = data.startAngle
    this._penIcon = data.pen.penIcon
    this._penColor = data.pen.penColor
    this._setupCanvas()
  }

}
