/**
 * config.ts
 * This file defines the `Config` class which stores the configuration values for the app.
 * @prop none - This class does not accept any props.
 * @export default Config
 */

export default class Config {
    public width: number;
    public height: number;
    public startPosX: number;
    public startPosY: number;
    public startAngle: number;
    public penIcon: string;
    public penColor: string;

    constructor() {
        this.width = 10;
        this.height = 10;
        this.startPosX = 0;
        this.startPosY = 0;
        this.startAngle = 0;
        this.penIcon = 'arrow';
        this.penColor = 'black';
    }

    public init() {
        console.log('Init config');
    }

    public set(key: string, value: number | string) {
        if (key === "width" || key === "height") {
            this.width = parseInt(value as string);
            this.height = parseInt(value as string);
        }
        else if (key === "startPosX") {
            this.startPosX = parseInt(value as string);
        }
        else if (key === "startPosY") {
            this.startPosY = parseInt(value as string);
        } 
        else if (key === "startAngle") {
            this.startAngle = parseInt(value as string);
        } else if (key === "penIcon") {
            this.penIcon = value as string;
        } else if (key === "penColor") {
            this.penColor = value as string;
        }
        console.log('Config set : ', key, value);
    }

    /**
     * Save method
     * This method is called when the user clicks the save button
     */ 
    public save() {
        return {
            width: this.width,
            height: this.height,
            startPosX: this.startPosX,
            startPosY: this.startPosY,
            startAngle: this.startAngle,
            penIcon: this.penIcon,
            penColor: this.penColor
        }
    }

    /**
     * Load method
     * This method is called when the user clicks the load button or when the app is initialized
     */
    public load(data: { width: number; height: number; startPosX: number; startPosY: number; startAngle: number; penIcon: string; penColor: string; }) {
        this.width = data.width;
        this.height = data.height;
        this.startPosX = data.startPosX;
        this.startPosY = data.startPosY;
        this.startAngle = data.startAngle;
        this.penIcon = data.penIcon;
        this.penColor = data.penColor;
    }
        
}