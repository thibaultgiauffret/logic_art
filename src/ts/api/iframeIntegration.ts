/**
 * iframeIntegration.ts
 * API to interact with an IFrame, based on RPC
 * @export IFrameHandler
 * @export sendAssignmentDataToIFrame
 **/

import { RPC } from '@mixer/postmessage-rpc'
import { ActivityParams } from '../../types/iframeIntegration'
import { useEffect } from 'react'
import { useAppContext, AppContextType } from '../../context'

// Define the service ID
// Get the service ID from the URL parameters "serviceId"
const urlParams = new URLSearchParams(window.location.search)
const serviceId = urlParams.get('serviceId') || 'default'

// Create a new RPC instance
const rpc = new RPC({
    target: window.parent,
    serviceId,
    origin: '*'
})

//   Get the activity params from IFrame
async function setActivityParamsFromIFrame({ mode, activity, workflow, studentAssignment, assignmentData }: ActivityParams) {
    console.log('Activity params from IFrame : ', { mode, activity, workflow, studentAssignment, assignmentData })
}

// Trigger the custom event when the function "sendAssignmentDataToIFrame" is called
export async function sendAssignmentDataToIFrame() {
    // Dispatch the custom event
    const event = new Event('sendAssignmentDataToIFrame');
    window.dispatchEvent(event);
}

export const IFrameHandler: React.FC = () => {
    const { blocks, config } = useAppContext() as AppContextType

    useEffect(() => {
        if (!config) {
            return
        }
        // Expose the platformGetActivityParams method to IFrame (to get the activity params)
        rpc.expose('platformGetActivityParams', async () => {
            return config
        })

        // Get the activity params from IFrame
        const fetchActivityParams = async () => {
            try {
                const activityParams = await rpc.call<ActivityParams>('toolGetActivityParams', {})
                setActivityParamsFromIFrame(activityParams)
            } catch (error) {
                console.error('Communication issue with IFrame : ', error)
            }
        }

        fetchActivityParams()
    }, [config])

    // Add an event listener to send the assignment data to IFrame (when sendAssignmentDataToIFrame is called)
    useEffect(() => {
        const handleSendAssignmentData = async () => {
            if (blocks) {
                const data = blocks.download();
                rpc.call('saveStudentAssignment', { result: data });
            }
        };

        // Listen for the custom event
        window.addEventListener('sendAssignmentDataToIFrame', handleSendAssignmentData);

        // Cleanup the event listener on component unmount
        return () => {
            window.removeEventListener('sendAssignmentDataToIFrame', handleSendAssignmentData);
        };
    }, [blocks]);

    return null
}
