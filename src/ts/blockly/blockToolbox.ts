/**
 * blockToolbox.ts
 * This file defines the toolbox configuration for Blockly, specifying the available categories and blocks.
 * @export toolbox
 */

const toolbox = {
  kind: 'categoryToolbox',
  contents: [
    {
      kind: 'category',
      name: 'Actions',
      colour: '210',
      contents: [
        {
          kind: 'block',
          type: 'forward'
        },
        {
          kind: 'block',
          type: 'backward'
        },
        {
          kind: 'block',
          type: 'turn_left'
        },
        {
          kind: 'block',
          type: 'turn_right'
        }
      ]
    },
    {
      kind: 'category',
      name: 'Drawing',
      colour: '120',
      contents: [
        {
          kind: 'block',
          type: 'draw_red'
        },
        {
          kind: 'block',
          type: 'draw_green'
        },
        {
          kind: 'block',
          type: 'draw_blue'
        },
        {
          kind: 'block',
          type: 'draw_yellow'
        }
      ]
    },
    {
      kind: 'category',
      name: 'Control',
      colour: '330',
      contents: [
        {
          kind: 'block',
          type: 'control_repeat'
        }
      ]
    },
    {
      kind: 'category',
      name: 'Others',
      colour: '210',
      contents: [
        {
          kind: 'block',
          type: 'base'
        }
      ]
    }
  ]
}

export { toolbox }
