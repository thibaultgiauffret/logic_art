/**
 * blockGenerators.ts
 * This file defines custom block generators for Blockly using the JavaScript generator.
 * The `setBlocksGen` function sets up these custom block generators.
 * @export setBlocksGen
 */

import { javascriptGenerator } from 'blockly/javascript'
import Blockly from 'blockly'

export function setBlocksGen () {

  // Base
  javascriptGenerator.forBlock['base'] = function () {
    const code = '{ "type": "base" }'
    return code
  }

  // Forward
  javascriptGenerator.forBlock['forward'] = function () {
    const code = '{ "type": "forward" }'
    return code
  }

  // Backward
  javascriptGenerator.forBlock['backward'] = function () {
    const code = '{ "type": "backward" }'
    return code
  }

  // Turn Right
  javascriptGenerator.forBlock['turn_right'] = function () {
    const code = '{ "type": "turnRight" }'
    return code
  }

  // Turn Left
  javascriptGenerator.forBlock['turn_left'] = function () {
    const code = '{ "type": "turnLeft" }'
    return code
  }

  // Draw red
  javascriptGenerator.forBlock['draw_red'] = function () {
    const code = '{ "type": "drawRed" }'
    return code
  }

  // Draw green
  javascriptGenerator.forBlock['draw_green'] = function () {
    const code = '{ "type": "drawGreen" }'
    return code
  }

  // Draw blue
  javascriptGenerator.forBlock['draw_blue'] = function () {
    const code = '{ "type": "drawBlue"}'
    return code
  }

  // Draw yellow
  javascriptGenerator.forBlock['draw_yellow'] = function () {
    const code = '{ "type": "drawYellow" }'
    return code
  }

  // Repeat
  javascriptGenerator.forBlock['control_repeat'] = function (block: Blockly.Block) {
    // Get the field number of repeats
    const repeats = block.getFieldValue('TIMES')
    const branch = javascriptGenerator.statementToCode(block, 'DO')
    // Remove \n in branch
    const inlineBranch = branch.replace(/\n/g, '')
    const code = '{ "type": "repeat", "times": ' + repeats + ', "branch": [' + inlineBranch + '] }'
    return code
  }

  return javascriptGenerator
}
