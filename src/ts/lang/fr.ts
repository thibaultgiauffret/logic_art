const fr = {
    // Actions
    "FORWARD": "Avancer",
    "BACKWARD": "Reculer",
    "TURN_RIGHT": "Droite",
    "TURN_LEFT": "Gauche",

    // Draw 
    "DRAW_RED": "Colorier en rouge",
    "DRAW_BLUE": "Colorier en bleu",
    "DRAW_GREEN": "Colorier en vert",
    "DRAW_YELLOW": "Colorier en jaune",

    // Controls
    "REPEAT": "Répéter",
    "TIMES": "fois"
};

export default fr;