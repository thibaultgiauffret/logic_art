/**
 * blocks.ts
 * This file defines the `Blocks` class, which initializes and manages the Blockly workspace.
 * The class provides methods to interact with the Blockly workspace, such as generating code, downloading the workspace state, and resetting the workspace.
 * @prop blockDiv - A reference to the div element containing the Blockly workspace.
 * @export default Blocks
 */

import * as Blockly from 'blockly'
import { JavascriptGenerator } from 'blockly/javascript'
import fr from './lang/fr'
import { toolbox } from './blockly/blockToolbox'
import { setBlocksGen } from './blockly/blockGenerators'
import { setBlockList } from './blockly/blockList'

export default class Blocks {

  private _blockDiv: HTMLDivElement

  constructor(blocksDiv: HTMLDivElement) {
    this._blockDiv = blocksDiv
  }

  private _toolbox: Blockly.Toolbox | Blockly.IToolbox | null = null
  public _workspace!: Blockly.WorkspaceSvg
  private _javascriptGenerator: JavascriptGenerator | null = null
  private _injectionDiv: HTMLElement = document.createElement('div')
  private _code: string = ''
  private _customDark: Blockly.Theme = Blockly.Theme.defineTheme('default', {
    name: ''
  })
  private _style: Blockly.Theme = Blockly.Themes.Classic
  private _toolboxFlag = true

  /**
   * Initializes the Blockly workspace.
   * @async
   * @returns {Promise<void>} A Promise that resolves when the workspace is initialized.
   */
  public async init() {
    console.log('Initializing Blockly workspace')
    this.setDarkTheme()
    this.patchRightBump()
    Blockly.setLocale(fr)
    this.createWorkspace()
  }

  public createWorkspace() {
    console.log('Creating workspace')
    const options = {
      toolbox,
      collapse: true,
      comments: true,
      disable: true,
      maxBlocks: Infinity,
      trashcan: true,
      horizontalLayout: false,
      toolboxPosition: 'start',
      css: true,
      media: 'https://blockly-demo.appspot.com/static/media/',
      rtl: false,
      scrollbars: true,
      sounds: true,
      oneBasedIndex: true,
      grid: {
        spacing: 20,
        length: 1,
        colour: '#888',
        snap: false
      },
      zoom: {
        controls: true,
        wheel: true,
        startScale: 1,
        maxScale: 3,
        minScale: 0.3,
        scaleSpeed: 1.2
      },
      renderer: 'zelos',
      theme: this._style
    }

    this._workspace = Blockly.inject(this._blockDiv, options)
    this._workspace.addChangeListener(Blockly.Events.disableOrphans)
    this._toolbox = this._workspace.getToolbox()

    this.setBlocksColor()
    this.toolboxStyle()
    this.initTooltips()
    this.setBlocks()

    this._injectionDiv = this._workspace.getInjectionDiv() as HTMLElement
    this._injectionDiv.classList.add('rounded-md')

    // Refresh the workspace to display the blocks (used to avoid toolbox panel cliping)
    Blockly.svgResize(this._workspace)
  }

  /**
   * Adds a new base block to the Blockly workspace.
   * @returns {void}
   */
  public addBase() {
    const newBlock = this._workspace.newBlock('base')
    newBlock.initSvg()
    newBlock.render()
    this._workspace.centerOnBlock(newBlock.id)
    newBlock.moveBy(0, -100)
  }

  /**
   * Patches the right bump of the Blockly blocks in zelos theme.
   * These patches require modifications to the Blockly lib :
   * blockly/core/renderers/zelos/info.d.ts with adjustXPosition_() from protected to public
   * blockly/core/renderers/common/info.d.ts with makeSpacerRow_ from protected to public, and constants_ from protected to public
   * These modification should be done with patch-package on postinstall via npm install.
   * @returns {void}
   */
  public patchRightBump() {

    // // Removes the auto x adjust of the block's content
    // Blockly.zelos.RenderInfo.prototype.adjustXPosition_ = function () {
    //   // do nothing
    // };

    // Patches the vertical spacing inside blocks to avoid clipping (not working well, creating a vertical shift between start and end of a statement block... wtf ?)
    // const origMakeSpacerRow_ = Blockly.blockRendering.RenderInfo.prototype.makeSpacerRow_;
    // Blockly.blockRendering.RenderInfo.prototype.makeSpacerRow_ = function () {
    //   const spacer = origMakeSpacerRow_.apply(this, [
    //     this.topRow,
    //     this.bottomRow,
    //   ]);
    //   spacer.height/=4;
    //   return spacer;
    // };
  }

  /**
   * Sets the Blockly workspace to use a custom dark theme.
   * @returns {void}
   */
  public setDarkTheme() {
    const blockStyles = {
      hat_blocks: {
        hat: 'cap'
      }
    }

    this._customDark = Blockly.Theme.defineTheme('customdark', {
      name: 'customdark',
      base: Blockly.Themes.Classic,
      componentStyles: {
        workspaceBackgroundColour: '#1f2937',
        toolboxBackgroundColour: '#374151',
        toolboxForegroundColour: '#fff',
        flyoutBackgroundColour: '#252526',
        flyoutForegroundColour: '#ccc',
        flyoutOpacity: 0.5,
        scrollbarColour: '#333',
        scrollbarOpacity: 0.5,
        insertionMarkerColour: '#333',
        insertionMarkerOpacity: 1,
        markerColour: '#333',
        cursorColour: '#333',
        selectedGlowColour: '#333',
        selectedGlowOpacity: 1,
        replacementGlowColour: '#333',
        replacementGlowOpacity: 1
      },
      blockStyles
    })
  }

  /**
   * Changes the Blockly workspace theme to the specified theme.
   * @param {string} theme - The name of the theme to change to ("light" or "dark").
   * @returns {void}
   */
  public changeTheme(isDarkMode: boolean) {
    console.log('Changing theme to', isDarkMode)
    if (!isDarkMode) {
      console.log('Changing to light theme')
      this._style = Blockly.Themes.Classic
    } else {
      console.log('Changing to dark theme')
      this._style = this._customDark
    }
    this._workspace.setTheme(this._style)
  }

  /**
   * Changes the Blockly workspace theme to the specified theme.
   * @returns {void}
   */
  public toolboxStyle() {
    const titres = [
      'Mouvement',
      'Dessin',
      'Répétition',
      'Autres'
    ]
    const icons = [
      '<i class="fa-solid fa-arrow-up"></i>',
      '<i class="fa-solid fa-paint-brush"></i>',
      '<i class="fa-solid fa-redo"></i>',
      '<i class="fa-solid fa-ellipsis-h"></i>'
    ]
    const category = (this._toolbox as Blockly.Toolbox).getToolboxItems() as Blockly.IToolboxItem[]
    for (let i = 0; i < category.length; i++) {
      const div = category[i].getDiv()
      if (div) {
        div.getElementsByClassName(
          'blocklyTreeLabel'
        )[0].innerHTML =
          icons[i] +
          '&nbsp;&nbsp;' +
          titres[i]
      }
    }
  }

  /**
   * Generates code from the Blockly workspace.
   * @returns {void}
   */
  public getCode() {
    console.log('Generating code')

    if (!this._workspace) {
      console.error('Workspace is not initialized');
      return '';
    }

    // List all blocks in the workspace
    const allBlocks = this._workspace.getAllBlocks()
    console.log('All blocks', allBlocks)

    this._code = ''
    const otherBlocks = this._workspace
      .getTopBlocks(true)
      .filter((block) => block.type !== 'function_def')
    // Generate code from allBlocks with blockToCode
    otherBlocks.map((block) => {
      console.log('Generating code for block', block)
      // get code from block function_def even if it is orphan
      if (this._javascriptGenerator) {
        const code = this._javascriptGenerator.blockToCode(block)
        // Add to this._code
        this._code += code
        return code
      } else {
        console.error('Javascript generator is not initialized')
        return ''
      }

    })
    console.log('Generated code', this._code)
    return this._code
  }

  /**
   * Downloads the Blockly workspace as an XML string. This method is used to save the workspace state.
   * @returns {string} The XML string representing the current state of the workspace.
   */
  public download() {
    const xml = Blockly.Xml.workspaceToDom(this._workspace)
    const xmlText = Blockly.Xml.domToText(xml)
    return xmlText
  }

  /**
   * Uploads the Blockly workspace state from an XML string.
   * @param {any} state - The XML string representing the state of the workspace to upload.
   * @returns {void}
   */
  public upload(state: string) {
    this._workspace.clear()
    const xml = Blockly.utils.xml.textToDom(state)
    Blockly.Xml.domToWorkspace(xml, this._workspace)
    // set workspace center to the top block location
    const topBlocks = this._workspace.getTopBlocks(true)
    if (topBlocks.length > 0) {
      const topBlock = topBlocks[0]
      const topBlockXY = topBlock.getRelativeToSurfaceXY()
      this._workspace.scrollX = -topBlockXY.x
      this._workspace.scrollY = -topBlockXY.y
    }
    this._workspace.getTopBlocks(true)[0].moveBy(50, 50)
  }

  /**
   * Toggles the visibility of the Blockly toolbox.
   * @returns {void}
   */
  public async hideToolbox() {
    if (this._toolboxFlag === false) {
      this._toolboxFlag = true
      this._workspace.getToolbox()!.setVisible(true)
    } else {
      this._toolboxFlag = false
      this._workspace.getToolbox()!.setVisible(false)
      this._workspace.hideChaff()
    }
  }

  /**
   * Undoes the last action in the Blockly workspace.
   * @returns {void}
   */
  public async undo() {
    this._workspace.undo(false)
  }

  /**
   * Redoes the last undone action in the Blockly workspace.
   * @returns {void}
   */
  public async redo() {
    this._workspace.undo(true)
  }

  /**
   * Resets the Blockly workspace to its initial state.
   * @returns {void}
   */
  public async reset() {
    this._workspace.clear()
    this.addBase()
  }

  /**
   * Initializes custom tooltips for Blockly blocks.
   * @returns {void}
   */
  public async initTooltips() {
    const customTooltip = function (div: Element, element: Element) {
      if (element instanceof Blockly.BlockSvg) {
        div.classList.add('ui-tooltip')
      }
      const tip = Blockly.Tooltip.getTooltipOfObject(element)
      const text = document.createElement('div')
      text.style.color = 'white'
      text.style.fontSize = '14px'
      text.style.padding = '2px'
      text.textContent = tip
      const container = document.createElement('div')
      container.style.display = 'flex'
      container.appendChild(text)
      div.appendChild(container)
    }
    Blockly.Tooltip.setCustomTooltip(customTooltip)
  }

  /**
   * Defines the custom Blockly blocks used in the workspace.
   * @returns {void}
   */
  public setBlocks() {

    setBlockList()
    this._javascriptGenerator = setBlocksGen()

    // Add the base block to the workspace
    this.addBase()

  }

  /**
   * Defines the custom colors used for Blockly blocks.
   * @returns {void}
   */
  public setBlocksColor() {
    Blockly.Msg.BKY_ACTION_COLOR = '#42af55'
    Blockly.Msg.BKY_EVENT_COLOR = '#f2b134'
    Blockly.Msg.BKY_CONTROL_COLOR = '#f27474'
    Blockly.Msg.BKY_SENSING_COLOR = '#5c9bd1'
  }
}
