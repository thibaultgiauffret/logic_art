/**
 * gui.ts
 * This file defines the `GUI` class which manages the main graphical user interface of the application.
 * The class provides methods for initializing the GUI components and handling their interactions.
 * @prop blocks: Blocks - The blocks interface of the application.
 * @prop grid: Grid - The grid interface of the application.
 * @export default GUI
 */

import Blocks from "./blocks";
import Grid from "./grid";
import Config from "./config";

/**
 * Main GUI class
 * It initializes all the app parts : blockly editor, grid, etc.
 */
export default class GUI {

    public mode: string | null;
    public blocks: Blocks;
    private _blocksDiv: HTMLDivElement;
    public grid: Grid;
    private _gridDiv: HTMLDivElement;
    public objectiveGrid: Grid | undefined;
    private _objectiveGridDiv: HTMLDivElement | undefined;
    public config?: Config;

    constructor(blocksDiv: HTMLDivElement, gridDiv: HTMLDivElement) {

        console.log('Init GUI');

        // Check for mode in url
        const urlParams = new URLSearchParams(window.location.search);
        const mode = urlParams.get('mode');
        this.mode = mode;

        console.log('Mode:', mode);

        // Clear blocks and grid divs content
        blocksDiv.innerHTML = '';
        gridDiv.innerHTML = '';

        // Initialize blocks and grid interfaces
        this._blocksDiv = blocksDiv;
        this.blocks = new Blocks(this._blocksDiv);
        this._gridDiv = gridDiv;
        this.grid = new Grid(this._gridDiv);
    }

    public initGUI() {
        console.log('Init GUI');
        this.blocks.init();
        this.grid.init();
    }

    public addObjectiveGrid(objectiveGridDiv: HTMLDivElement) {
        console.log('Init objective grid');
        objectiveGridDiv.innerHTML = '';
        this._objectiveGridDiv = objectiveGridDiv;
        this.objectiveGrid = new Grid(this._objectiveGridDiv, true);
    }
}