/**
 * save.ts
 * This file defines the `saveToLocalStorage` function which saves the current state of the app to the local storage.
 */

// Import the required classes and their save methods
import Config from '../config';
import Grid from '../grid';
import Blocks from '../blocks';

// Save format is JSON :
// {
//     date: date,
//     config: config_params_object,
//     grid: grid_params_object,
//     blocks: xml_string
// }

function saveToLocalStorage(config: Config, grid: Grid, blocks: Blocks) {
    // Create the save object
    const saveObject = {
        date: new Date(),
        config: config.save(),
        grid: grid.save(),
        blocks: blocks.download()
    }

    // Save the object to the local storage (list of 10 last saves)
    let saves = JSON.parse(localStorage.getItem('logicart_saves') || '[]');
    // Check if the save is already in the list (last save is the same as the current one, except for the date)
    if (saves.length > 0) {
        const lastSave = saves[saves.length - 1];
        if (JSON.stringify(lastSave.config) === JSON.stringify(saveObject.config) && JSON.stringify(lastSave.grid) === JSON.stringify(saveObject.grid) && lastSave.blocks === saveObject.blocks) {
            // Do not save the same state twice
            console.log('Same state, not saved');
            return { status: '' }
        }
    }

    saves.push(saveObject);
    if (saves.length > 10) {
        saves.shift();
    }
    localStorage.setItem('logicart_saves', JSON.stringify(saves));

    return { status: "success" }; // TODO : improve this function to return false if an error occurs
}

async function saveToFile(config: Config, grid: Grid, blocks: Blocks) {
    // Create the save object
    const saveObject = {
        date: new Date(),
        config: config.save(),
        grid: grid.save(),
        blocks: blocks.download()
    }
    // Date and time for the file name
    const date = new Date();
    const dateStr = date.toISOString().split('T')[0] + '_' + date.toTimeString().split(' ')[0].replace(/:/g, '-');

    // Save the object to a file
    const blob = new Blob([JSON.stringify(saveObject)], { type: 'application/json' });
    const url = URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'logicart_save_' + dateStr + '.json';
    a.click();

    return true; // TODO : improve this function to return false if an error occurs
}

export { saveToLocalStorage, saveToFile };