/**
 * load.ts
 * This file defines the `loadFromLocalStorage` function which loads the last saved state of the app from the local storage.
 */

// Import the required classes and their load methods
import Config from '../config';
import Grid from '../grid';
import Blocks from '../blocks';

// Load format is JSON :
// {
//     date: date,
//     config: config_params_object,
//     grid: grid_params_object,
//     blocks: xml_string
// }

function loadFromLocalStorage(config: Config,
    grid: Grid,
    blocks: Blocks) {
    console.log('Load from local storage');
    // Load the object from the local storage
    let saves = JSON.parse(localStorage.getItem('logicart_saves') || '[]');
    if (saves.length > 0) {
        const saveObject = saves[saves.length - 1];
        console.log('Loaded save:', saveObject);
        config.load(saveObject.config);
        grid.load(saveObject.grid);
        blocks.upload(saveObject.blocks);
    }
}

async function loadFromFile(text:string, config: Config,
    grid: Grid,
    blocks: Blocks) {
    console.log('Load from file');
    // Load the object from the file
    const saveObject = JSON.parse(text);
    console.log('Loaded save:', saveObject);
    config.load(saveObject.config);
    grid.load(saveObject.grid);
    blocks.upload(saveObject.blocks);

    return true; // TODO : improve this function to return false if an error occurs
}

export { loadFromLocalStorage, loadFromFile };