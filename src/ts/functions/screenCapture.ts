/**
 * screenCapture.ts
 * This file defines the `screenCapture` function which captures the chosen element and saves it as an image.
 */

import html2canvas from 'html2canvas';

async function screenCapture(element: string): Promise<void> {
    return new Promise((resolve, reject) => {
        const elementToCapture = document.getElementById(element);
        if (elementToCapture) {
            html2canvas(elementToCapture).then((canvas) => {
                const link = document.createElement('a');
                const now = new Date();
                const formattedDate = `${String(now.getDate()).padStart(2, '0')}-${String(now.getMonth() + 1).padStart(2, '0')}-${String(now.getFullYear()).slice(-2)}_${String(now.getHours()).padStart(2, '0')}-${String(now.getMinutes()).padStart(2, '0')}`;
                link.download = `capture_logicart_${formattedDate}.png`;
                link.href = canvas.toDataURL();
                link.click();
                resolve();
            }).catch((error) => {
                reject(error);
            });
        } else {
            reject('Element not found');
        }
    });
}

export default screenCapture;