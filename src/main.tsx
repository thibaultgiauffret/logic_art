import { createRoot } from 'react-dom/client';
import React from 'react';
import AppWithProvider from './appWithProvider';

createRoot(document.getElementById('root')!).render(
    <React.StrictMode>
    <AppWithProvider />
  </React.StrictMode>
)
