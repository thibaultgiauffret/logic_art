import { useEffect, useRef } from 'react'

import { PrimeReactProvider } from 'primereact/api';
import Tailwind from 'primereact/passthrough/tailwind';

import Topbar from './components/topbar'
import BlocksDiv from './components/blocksDiv'
import { GridDiv, ObjectiveGridDiv } from './components/gridDiv'
import { ConfigDiv } from './components/configDiv'
import { Toast } from 'primereact/toast';

// Load the context
import { useAppContext, AppContextType } from './context'

// Load the IFrame handler
import { IFrameHandler } from './ts/api/iframeIntegration';

import { saveToLocalStorage } from './ts/functions/save';
import { loadFromLocalStorage } from './ts/functions/load';

import '@fortawesome/fontawesome-free/css/all.min.css';
import 'primeicons/primeicons.css';

import './layers.scss'
import './app.scss'

function App() {

  console.log('App render');

  // Init toast
  const toast = useRef<Toast>(null);

  // Load the context with gui as argument
  const { isFirstRender, setIsFirstRender, blocks, grid, config, mode, gui } = useAppContext() as AppContextType;

  /**
   * Load the last saved state from the local storage
   */
  useEffect(() => {

    const executeCodeAsync = async () => {

      // If the config, grid and blocks are not null and it's the first render
      if (config !== null && grid !== null && blocks !== null && isFirstRender) {

        // Load the last saved state from the local storage
        loadFromLocalStorage(config, grid, blocks);

        // Set the first render to false
        setIsFirstRender(false);

        // Get the code
        const code = blocks?.getCode();

        // Run the code
        const pen = grid !== null ? grid.pen : null;
        try {
          const result = await pen?.executeCode(code!, false);
          if (!result?.status) {
            if (toast.current !== null) {
              toast.current!.show({ severity: 'error', summary: 'Attention', detail: result?.message });
            }
          }
        } catch (e) {
          console.error('Error executing code : ', e);
        }
      }
    };

    executeCodeAsync();
  }, [config, grid, blocks, isFirstRender, setIsFirstRender]);

  /**
   * Save every 2 minutes (TODO : improve this with user parameters)
   */
  useEffect(() => {
    const interval = setInterval(() => {
      if (config !== null && grid !== null && blocks !== null) {
        const state = saveToLocalStorage(config!, grid!, blocks!);
        if (state.status === "success") {
          toast.current?.show({ severity: 'success', summary: 'Sauvegarde dans le navigateur', detail: 'Sauvegarde réussie.' });
        } else if (state.status === "error") {
          toast.current?.show({ severity: 'error', summary: 'Sauvegarde dans le navigateur', detail: 'Erreur lors de la sauvegarde.' });
        }
      }
    }, 120000);
    return () => clearInterval(interval);
  }, [config, grid, blocks]);



  // Handle after render (to resize grids)
  const afterRender = () => {
    if (gui !== null) {
      gui.grid.resizeCanvas();
    }

  }

  useEffect(() => {
    afterRender();
  });

  return (
    <>
      <IFrameHandler />
      <PrimeReactProvider value={{ unstyled: true, pt: Tailwind }}>

        <Toast ref={toast} />
        {/* Topbar here */}
        <Topbar />

        {/* Config here */}

        {/* Two columns */}
        <div className="mainContainer grid grid-cols-1 md:grid-cols-2 w-full" id="content">
          {/* Blockly component here */}
          <BlocksDiv />
          {/* Grid component here */}
          <div className="h-screen md:h-full">

            {mode === "config" &&
              <ConfigDiv />
            }

            <GridDiv />

            {mode === "objective" &&
              <ObjectiveGridDiv />
            }

          </div>
        </div>
      </PrimeReactProvider>
    </>
  )
}

export default App
